from django.shortcuts import render
from django.views.generic.edit import UpdateView,CreateView,DeleteView
from django.core.urlresolvers import reverse_lazy,reverse
from django.http import HttpResponse
# Create your views here.

from matches.models import Matches

def matches(request):
    all_matches = sorted(Matches.objects.all(), key=lambda match : match.path)
    context= { 'all_matches':all_matches }
    return render(request, 'matches/matches.html', context)

class MatchUpdate(UpdateView):
    model = Matches
    fields = ['regex', 'type', 'path']
    template_name_suffix = '_update'
    success_url= '/matches/'

class MatchCreate(CreateView):
    model = Matches
    fields = ['regex', 'type', 'path']
    template_name_suffix = '_create'
    success_url= '/matches/'

class MatchDelete(DeleteView):
    model = Matches
    success_url= '/matches/'
