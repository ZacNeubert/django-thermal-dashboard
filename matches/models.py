# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Matches(models.Model):
    matchid = models.AutoField(db_column='MatchID', primary_key=True)  # Field name made lowercase.
    regex = models.CharField(db_column='Regex', max_length=200)  # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=200)  # Field name made lowercase.
    path = models.CharField(db_column='Path', max_length=200)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'matches'

    def __str__(self):
        return self.regex+"\t"+self.path
