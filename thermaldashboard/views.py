from django.http import HttpResponse
from django.shortcuts import render
from .models import Sensor,Sensordata

def dash(request):
	all_sensors = Sensor.objects.all()
	context = {'all_sensors' : all_sensors }
	return render(request, 'thermaldashboard/dash.html', context)

def history(request, pk):
	print "Hit history function"
	sensor = Sensor.objects.get(pk=pk)
	context = {'sensor' : sensor }
	return render(request, 'thermaldashboard/history.html', context)
