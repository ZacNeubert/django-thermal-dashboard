from django.contrib import admin
from thermaldashboard.models import Sensor,Sensordata

admin.site.register(Sensor)
admin.site.register(Sensordata)
