"""thermaldashboard URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,patterns,include
from django.contrib import admin

from . import views


app_name='thermaldashboard'
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^dash/', views.dash, name='dash'),
    url(r'^$', views.dash, name='dash'),
    url(r'^(?P<pk>[0-9]+)/history/', views.history, name='history'),
    url(r'^matches/', include('matches.urls')),
    url(r'^pimote/', include('pimote.urls')),
]
