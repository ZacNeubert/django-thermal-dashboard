# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models
from datetime import timedelta

class Sensor(models.Model):
    sensorid = models.IntegerField(db_column='SensorID', primary_key=True)  # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=255)  # Field name made lowercase.
    sensortype = models.CharField(db_column='Type', max_length=255, blank=True, null=True)  # Field name made lowercase.
    color = models.CharField(db_column='Color', max_length=255,blank=True, null=True)

    class Meta:
        db_table = 'Sensor'

    def topOne(self):
	sensordatas = Sensordata.forSensor(self.sensorid)[:1]
	if len(sensordatas) > 0:
		return sensordatas[0]
	return None

    def current(self):
	return self.topOne().displaydata

    def topThree(self):
	sensordatas = Sensordata.forSensor(self.sensorid)[:5]
	return sensordatas

    def allData(self):
	sensordatas = Sensordata.forSensor(self.sensorid)
	return sensordatas

    def getImage(self):
	return 'polls/sensoricons/half'+self.sensortype+'.png'


class Sensordata(models.Model):
    messageid = models.IntegerField(db_column='MessageID', primary_key=True)  # Field name made lowercase.
    sensorid = models.IntegerField(db_column='SensorID')  # Field name made lowercase.
    date = models.DateTimeField(db_column='Date')  # Field name made lowercase.
    battery = models.FloatField(db_column='Battery')  # Field name made lowercase.
    plotvalue = models.CharField(db_column='PlotValue', max_length=255)  # Field name made lowercase.
    gatewayid = models.IntegerField(db_column='GatewayID')  # Field name made lowercase.
    displaydata = models.CharField(db_column='DisplayData', max_length=255)  # Field name made lowercase.

    class Meta:
        db_table = 'SensorData'

    def getDate(self):
	return self.date + timedelta(0,0,0,0,0,6)

    @staticmethod
    def forSensor(sid):
	print sid
	results = Sensordata.objects.filter(sensorid=sid).order_by('-date')
	return results


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'
