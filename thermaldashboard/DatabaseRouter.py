#!/usr/bin/python

class DatabaseRouter(object):
    def db_for_write(self, model, **hints):
        print model._meta.app_label
        if model._meta.app_label == 'matches':
                return 'matches'
        return None

    def db_for_read(self, model, **hints):
        print model._meta.app_label
        if model._meta.app_label == 'matches':
                return 'matches'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if obj1._meta.app_label == 'matches' or obj2._meta.app_label == 'matches':
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label == 'matches':
            return db=='matches'
        return None
